import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlateauIaComponent } from './plateau-ia.component';

describe('PlateauIaComponent', () => {
  let component: PlateauIaComponent;
  let fixture: ComponentFixture<PlateauIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlateauIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlateauIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
