import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'plateau-ia',
  templateUrl: './plateau-ia.component.html',
  styleUrls: ['./plateau-ia.component.scss']
})
export class PlateauIaComponent implements OnInit {

  @Input() public gridIA: Array<Array<number>>;
  @Output() public updateScoreEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
