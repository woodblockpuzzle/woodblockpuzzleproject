import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPiece } from '../model/entities/interfaces/piece';
import { EngineService } from '../model/services/engine.service';
import { ComponentCommunicationService } from '../model/services/component-communication.service';
import { IPlateau } from '../model/entities/interfaces/plateau';

@Component({
  selector: 'piece-posable-ia',
  templateUrl: './piece-posable-ia.component.html',
  styleUrls: ['./piece-posable-ia.component.scss']
})
export class PiecePosableIaComponent implements OnInit {

  @Input() public blockPosableIA: Array<any> = [];
  @Input() public coordonnees: Array<Array<number>> = [];
  @Input() public gridIA: IPlateau;

  @Output() public getPieceEvent = new EventEmitter();

  constructor(private compComService: ComponentCommunicationService, private engineService: EngineService) {

    this.compComService.getStatusIAGame().subscribe(
      status => {
        if (status)
          this.startIAGame();
        else
          this.endIAGame();
      });
  }

  ngOnInit() { }


  async startIAGame() {
    await this.delay(1000);
    this.updateGrilleIA();
  }

  endIAGame() {

  }

  delay = (amount: number) => {
    return new Promise((resolve) => {
      setTimeout(resolve, amount);
    });
  };

  async updateGrilleIA() {
    let piece: IPiece;
    let cible: Array<number>;

    while ((piece = this.blockPosableIA.shift()) && (cible = this.coordonnees.shift())) {

      let values: Array<Array<number>> = piece.values; // récupére la matrice de la piéce

      for (let i: number = 0; i < values.length; i++) {
        let k: number = +cible[0] + i; // décaler par rapport aux lignes
        for (let j: number = 0; j < values[i].length; j++) {
          let l: number = +cible[1] + j; // décaler par rapport aux colonnes
          try {
            this.gridIA.mainTable[k][l] = values[i][j] ? values[i][j] : this.gridIA.mainTable[k][l];
          } catch (error) {
            console.log(error);
          }
        }
      }

      // remplacer -2 par 0 pour pas affecter le nouveau calcul du score
      for (let i: number = 0; i < this.gridIA.mainTable.length; i++) {
        for (let j: number = 0; j < this.gridIA.mainTable[i].length; j++) {
          this.gridIA.mainTable[i][j] = this.gridIA.mainTable[i][j] == -2 ? 0 : this.gridIA.mainTable[i][j];
        }
      }
      await this.delay(500);

      this.engineService.verifierPlateau(this.gridIA).subscribe(plateau => {
        this.gridIA = plateau;
        this.compComService.setScoreIA(this.gridIA.score);
      });
      
      await this.delay(1000);
    }
    this.getPieceEvent.emit('');

  }

}
