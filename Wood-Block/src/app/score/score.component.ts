import { Component, OnInit, Input } from '@angular/core';
import { ComponentCommunicationService } from '../model/services/component-communication.service';

@Component({
  selector: 'score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  @Input() public scoreJoueur: number;
  @Input() public scoreIA: number;

  constructor(private compComService: ComponentCommunicationService) {
    this.compComService.getScoreIA().subscribe(score => {
      this.scoreIA = score;
    });
  }

  ngOnInit() {
  }

}
