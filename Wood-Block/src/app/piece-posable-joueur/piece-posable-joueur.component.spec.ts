import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecePosableComponent } from './piece-posable-joueur.component';

describe('PiecePosableComponent', () => {
  let component: PiecePosableComponent;
  let fixture: ComponentFixture<PiecePosableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiecePosableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecePosableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
