import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DropEvent} from 'ng-drag-drop';
import {ComponentCommunicationService} from '../model/services/component-communication.service';
import {IPiece} from '../model/entities/interfaces/piece';
import {EngineService} from '../model/services/engine.service';

@Component({
  selector: 'piece-posable-joueur',
  templateUrl: './piece-posable-joueur.component.html',
  styleUrls: ['./piece-posable-joueur.component.scss']
})
export class PiecePosableJoueurComponent implements OnInit {

  @Input() public blockPosableJoueur: Array<any> = [];
  @Input() public gameOverJoueur: boolean;
  @Output() public startIAEvent = new EventEmitter();

  private pieceToDelete: IPiece;

  constructor(private compComService: ComponentCommunicationService, private engineService: EngineService) {
    this.compComService.getPieceJoueur().subscribe(piece => {
      if (piece) {
        this.removePiece(piece);
      } else {
      }
    });
  }

  ngOnInit() {
  }

  onDrop(e: DropEvent) {
    console.log(e.dragData);
  }

  onDragStart(e: any) {
    console.log(e);
    console.log('drag start: prévisualisation');
    this.compComService.makePreview(e);
  }

  // enlever la piéce posée de la liste des piéces disponibles
  public removePiece(item: any): void {

    for(let i = 0; i<this.blockPosableJoueur.length; i++){
      if(this.blockPosableJoueur[i].nom === item.piece.nom) {
        this.blockPosableJoueur.splice(i,1);
        break;
      }
    }

    if (this.blockPosableJoueur.length == 0) {
      this.startIAEvent.emit('');
    }
  }
}
