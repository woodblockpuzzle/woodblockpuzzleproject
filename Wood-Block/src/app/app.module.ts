import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';

import { ScoreComponent } from './score/score.component';
import { PlateauJoueurComponent } from './plateau-joueur/plateau-joueur.component';
import { PlateauIaComponent } from './plateau-ia/plateau-ia.component';

import { NgDragDropModule } from 'ng-drag-drop';
import { PiecePosableJoueurComponent } from './piece-posable-joueur/piece-posable-joueur.component';
import { PiecePosableIaComponent } from './piece-posable-ia/piece-posable-ia.component';

@NgModule({
  declarations: [
    AppComponent,
    ScoreComponent,
    PlateauJoueurComponent,
    PlateauIaComponent,
    PiecePosableJoueurComponent,
    PiecePosableIaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    NgDragDropModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
