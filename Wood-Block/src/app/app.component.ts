import {Component} from '@angular/core';
import {EngineService} from './model/services/engine.service';
import {IPlateau} from './model/entities/interfaces/plateau';
import {IPiece} from './model/entities/interfaces/piece';
import {ArtificialIntelligenceService} from './model/services/artificial-intelligence.service';
import {IReponseIA} from './model/entities/interfaces/reponseIA';
import {ComponentCommunicationService} from './model/services/component-communication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  public _plateauJoueur: IPlateau;
  public _plateauIA: IPlateau;
  public _piecesJoueur: IPiece[];
  public _piecesIA: IPiece[] = [];
  public _reponse: IReponseIA;
  public _coordonnees: Array<Array<number>> = [];

  public _gameOverJoueur: boolean;
  public _gameOverIA: boolean;

  public scoreJoueur: number; // score joueur
  public scoreIA: number; // score IA

  events: string[] = [];
  opened: boolean;

  constructor(private engineService: EngineService, private iaService: ArtificialIntelligenceService, private compComService: ComponentCommunicationService) {

    iaService.resetIA().subscribe(reset => console.log(reset));

    engineService.creerPlateau().subscribe(plateau => {
      this._plateauJoueur = plateau;
      this.scoreJoueur = this._plateauJoueur.score;
    });

    engineService.creerPlateau().subscribe(plateau => {
      this._plateauIA = plateau;
      this.scoreIA = this._plateauIA.score;
    });

    engineService.getRandomPiece().subscribe(pieces => {
      this._piecesJoueur = pieces;
      this._piecesIA = Object.assign(new Array<IPiece>(), this._piecesJoueur);
    });

  }

  public updateScoreJoueur(data: number): void {
    this.scoreJoueur = data;

    if (this._piecesJoueur.length > 0) {
      this.engineService.verifierPieces(this._plateauJoueur, this._piecesJoueur).subscribe(result => {
        this._gameOverJoueur = !result;

        if (this._gameOverJoueur) {
          this.startIA();
        }
      });
    }

  }

  public updateScoreIA(data: number): void {
    // this.scoreIA += +this._reponse.points;
    this.scoreIA = data;
  }

  public getPiece(): void {

    this.engineService.getRandomPiece().subscribe(piece => {
      console.log(`game over joueur ${this._gameOverJoueur}`);
      if (!this._gameOverJoueur) {
        this._piecesJoueur = piece;
      }

      let itemProcessed: number = 0;

      [0, 1, 2].forEach(i => {
        this.engineService.getPiece(piece[i].nom).subscribe(p => {
          this._piecesIA[i] = p;
          itemProcessed++;

          if (itemProcessed == 3 && this._gameOverJoueur) {
            console.log('declencher IA');
            this.startIA();
          }
        });
      });
    });
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '250px';
    console.log('openNav');
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    console.log('closeNav');
  }

  startIA() {
    if (!this._gameOverIA) {
      // Parse le JSON reçu pour ecraser piecesIA et generer un tableau des coordonnées des pieces
      this.iaService.startIA(this._piecesIA).subscribe(reponse => {
        this._reponse = reponse;

        if (this._reponse.status == "end") {
          this._gameOverIA = true;
          console.log("ia perd" + this._reponse.status);

          return;
        }

        try {

          for (let i = 0; i < 3; i++) {
            this._coordonnees[i] = [];
            for (let j = 0; j < 2; j++) {
              if (j === 0) {
                this._coordonnees[i][j] = this._reponse.pieces[i].x;
              } else {
                this._coordonnees[i][j] = this._reponse.pieces[i].y;
              }
            }
          }
          // console.log(this._coordonnees);

          let itemProcessed: number = 0;
          [0, 1, 2].forEach(i => {
            console.log(this._reponse.pieces);

            this.engineService.getPiece(this._reponse.pieces[i].nom).subscribe(piece => {
              itemProcessed++;
              this._piecesIA[i] = piece;

              if (itemProcessed == 3)
                this.compComService.startIAGame();
            });
          });
        } catch (error) {
          console.log(error);
        }
      });
    }
  }

  // refresh jeu
  reset(): void {
    window.location.reload();
  }

}

