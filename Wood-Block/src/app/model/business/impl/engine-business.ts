import {Plateau} from '../../entities/plateau';
import {IPiece} from '../../entities/interfaces/piece';

/*
 * Métier : EngineBusiness
 *
 * Gère toute la logique métier du jeu :
 *    - Création du plateau
 *    - Génération des  3 pièces jouables pour les joueurs
 *    - Calcul du score
 */
export class EngineBusiness {

  /*
   * Instanciation du plateau : Intancie le plateau et le score en début de partie
   */
  creerPlateau(): Plateau {
    let plateau: Plateau = new Plateau();
    plateau.mainTable = this.initializeMainTable();
    plateau.score = 0;
    return plateau;
  }

  /*
   * Initialisation du plateau : Remplit le plateau de 0 pour l'initialiser en début de partie
   */
  private initializeMainTable(): number[][] {
    let table: number[][];
    table = [];
    for (let i = 0; i < 8; i++) {
      table[i] = [];
      for (let j = 0; j < 8; j++) {
        table[i][j] = 0;
      }
    }
    return table;
  }

  /*
   * Vérification des lignes : Passe la valeur d'une ligne à -2 si la ligne est complétée
   */
  verifierPlateau(plateau: Plateau): Plateau {
    // plateau = FixturesRepo.genererPlateauFixe(); Seulement pour tester => à supprimer/commenter
    plateau.mainTable = this.verifierLignes(plateau.mainTable);
    plateau.mainTable = this.verifierColonnes(plateau.mainTable);
    plateau.score += this.calculeScore(plateau.mainTable);
    return plateau;
  }

  /*
   * Vérification des lignes : Passe la valeur d'une ligne à -2 si la ligne est complétée
   */
  private verifierLignes(mainTable: number[][]): number[][] {
    for (let i = 0; i < 8; i++) {
      let ok: boolean = true;
      for (let j = 0; j < 8; j++) {
        if (mainTable[i][j] === 0) {
          ok = false;
        }
      }
      if (ok) {
        for (let j = 0; j < 8; j++) {
          mainTable[i][j] = -2;
        }
      }
    }
    return mainTable;
  }

  /*
   * Vérification des colonnes : Passe la valeur d'une ligne à -2 si la colonne est complétée
   */
  private verifierColonnes(mainTable: number[][]): number[][] {
    for (let i = 0; i < 8; i++) {
      let ok: boolean = true;
      for (let j = 0; j < 8; j++) {
        if (mainTable[j][i] === 0) {
          ok = false;
        }
      }
      if (ok) {
        for (let j = 0; j < 8; j++) {
          mainTable[j][i] = -2;
        }
      }
    }
    return mainTable;
  }

  /*
   * Calcul du score avec la formule suivante : score = (2 x NbLignesColonnes - 1) * 80
   */
  private calculeScore(mainTable: number[][]): number {
    let points: number = 0;
    let lines: number = 0;
    // Calcul du nombre de lignes complètes
    for (let i = 0; i < 8; i++) {
      let row: boolean = true;
      for (let j = 0; j < 8; j++) {
        if (mainTable[i][j] !== -2) {
          row = false;
        }
      }
      if (row) {
        lines++;
      }
    }

    // Calcul du nombre de colonnes complètes
    for (let i = 0; i < 8; i++) {
      let col: boolean = true;
      for (let j = 0; j < 8; j++) {
        if (mainTable[j][i] !== -2) {
          col = false;
        }
      }
      if (col) {
        lines++;
      }
    }
    if (lines !== 0) {
      points = (2 * lines - 1) * 80;
    }
    return points;
  }

  /*
   * Génération d'un index aléatoire : Permet de générer 3 valeurs aléatoire qui permettront de determiner
   * l'index des pièces que vont devoir jouer les joueurs
   */
  getRandomIndex(): number[] {
    let randomIndex = new Array<number>(3);
    for (let i = 0; i < 3; i++) {
      randomIndex[i] = Math.floor(Math.random() * Math.floor(35));
    }
    return randomIndex;
  }

  /*
   * Vérification des pièces : Permet de vérifier si les pièces sont posables sur le plateau afin de savoir
   * si le joueur a perdu
   */
  verifierPieces(plateau: Plateau, pieces: IPiece[]): boolean {
    let isPieceJouable: boolean = false; // booléen pour savoir si la pièce testée peut être placée
    let nb_pieces = pieces.length;
    for (let index_piece = 0; index_piece < nb_pieces; index_piece++) {
      for (let ligne_plateau = 0; ligne_plateau < 8; ligne_plateau++) {
        for (let col_plateau = 0; col_plateau < 8; col_plateau++) {
          let nb_ligne_piece = pieces[index_piece].values.length;
          let nb_col_piece = pieces[index_piece].values[0].length;
          // Si le pièce que l'on veut placer ne déborde pas du plateau
          if (((ligne_plateau + nb_ligne_piece) < 8) && ((col_plateau + nb_col_piece) < 8)) {
            for (let ligne_piece = 0; ligne_piece < nb_ligne_piece; ligne_piece++) {
              for (let col_piece = 0; col_piece < nb_col_piece; col_piece++) {
                // Si le bloc de la pièce est plaçable (ie la case du plateau est libre ou la case de la piece est libre)
                if (!(plateau.mainTable[ligne_plateau + ligne_piece][col_plateau + col_piece] && pieces[index_piece].values[ligne_piece][col_piece])) {
                  isPieceJouable = true;
                } else { // Sinon une des deux cases n'est pas disponible
                  isPieceJouable = false;
                  break; // On ne teste pas la suite de la pièce, on sait déjà qu'elle n'est pas jouable pour le point d'origine [ligne_plateau][col_plateau]
                }
              }
              // La pièce n'est pas jouable, on ne teste pas le reste de la pièce
              if (!isPieceJouable) {
                break;
              }
            }
            // Si on trouve une pièce jouable alors on ne teste pas les autres
            // et on renvoit true pour dire que la partie n'est pas terminée (ie au moins une pièce peut encore être placée)
            if (isPieceJouable) {
              return true;
            }
          } else { // Si la pièce déborde alors elle n'est pas jouable
            isPieceJouable = false;
          }
        }
      }
    }
    // Aucune des pièces disponibles n'est jouable, on renvoit false
    return false;
  }
}
