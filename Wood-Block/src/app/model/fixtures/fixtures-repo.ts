import {Plateau} from '../entities/plateau';

export class FixturesRepo {

  static genererPlateauFixe(): Plateau {
    let plateau: Plateau;
    plateau = new Plateau();
    plateau.mainTable = this.genererTableauFixe();
    plateau.score = 0;
    return plateau;
  }

  static genererTableauFixe(): number[][] {
    let table: number[][];
    table = [
      [1, 1, 1, 1, 1, 1, 1, 1],
      [1, 1, 0, 1, 1, 1, 1, 1],
      [1, 1, 1, 1, 0, 1, 1, 1],
      [1, 1, 1, 0, 1, 1, 1, 1],
      [1, 1, 1, 1, 1, 0, 1, 1],
      [1, 0, 1, 1, 1, 1, 1, 1],
      [1, 1, 0, 1, 0, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 1]
    ];

    /* table = [
       [-2, -2, -2, -2, -2, -2, -2, -2],
       [-2, 1, 0, 1, 1, 1, 1, -2],
       [-2, 1, 1, 1, 0, 1, 1, -2],
       [-2, 1, 1, 0, 1, 1, 1, -2],
       [-2, 1, 1, 1, 1, 0, 1, -2],
       [-2, 0, 1, 1, 1, 1, 1, -2],
       [-2, 1, 0, 1, 0, 1, 0, -2],
       [-2, 0, 0, 0, 0, 0, 0, -2]
     ];*/
    return table;
  }
}
