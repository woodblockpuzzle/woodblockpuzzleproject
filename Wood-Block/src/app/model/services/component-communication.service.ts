import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { IPiece } from '../entities/interfaces/piece';

@Injectable({
  providedIn: 'root'
})
export class ComponentCommunicationService {

  private subjectJoueur = new Subject<any>();
  private subjectIA = new Subject<any>();
  private subjectIAStatus = new Subject<any>();
  private subjectIAScore = new Subject<any>();
  private subjectPreview = new Subject<any>();

  constructor() { }

  getPieceJoueur(): Observable<any> {
    return this.subjectJoueur.asObservable();
  }

  deletePieceJoueur(piece: IPiece) {
    this.subjectJoueur.next({ piece });
  }

  getPieceIA(): Observable<any> {
    return this.subjectIA.asObservable();
  }

  deletePieceIA(piece: IPiece) {
    this.subjectIA.next({ piece });
  }

  getPreview(): Observable<any> {
    return this.subjectPreview.asObservable();
  }

  makePreview(piece: any) {
    this.subjectPreview.next(piece);
  }

  getStatusIAGame(): Observable<any> {
    return this.subjectIAStatus.asObservable();
  }

  getScoreIA(): Observable<any> {
    return this.subjectIAScore.asObservable();
  }

  setScoreIA(score: number) {
    this.subjectIAScore.next(score);
  }

  startIAGame() {
    this.subjectIAStatus.next(true);
  }

  endIAGame() {
    this.subjectIAStatus.next(false);
  }
}
