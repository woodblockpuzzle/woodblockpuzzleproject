import {Injectable} from '@angular/core';
import {IPiece} from '../entities/interfaces/piece';
import {Observable} from 'rxjs';
import {IReponseIA} from '../entities/interfaces/reponseIA';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ArtificialIntelligenceService {

  private restAPIUrl = 'http://localhost:8080/compute?';
  private restAPIUrlReset = 'http://localhost:8080/reset';

  constructor(private http: HttpClient) {
  }

  startIA(_piecesIA: IPiece[]): Observable<IReponseIA> {
    return this.http.get<IReponseIA>(this.restAPIUrl + 'p1=' + _piecesIA[0].nom + '&p2=' + _piecesIA[1].nom + '&p3=' + _piecesIA[2].nom)
      .pipe(map((reponseIA: IReponseIA) => reponseIA)
      );
  }

  resetIA(): Observable<string> {
    return this.http.get<string>(this.restAPIUrlReset);
  }

}

