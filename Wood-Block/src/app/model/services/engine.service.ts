import {Injectable} from '@angular/core';
import {EngineBusiness} from '../business/impl/engine-business';
import {Plateau} from '../entities/plateau';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IPiece} from '../entities/interfaces/piece';
import {map} from 'rxjs/operators';


/*
 * Service : EngineService
 *
 * Service permettant d'accéder à la logique métier du jeu à savoir :
 *    - La gestion du score
 *    - La gestion du plateau
 *    - La gestion des pièces jouables
 */
@Injectable({
  providedIn: 'root'
})
export class EngineService {

  /*
   * engineBusiness : Logique métier du jeu
   */
  private engineBusiness: EngineBusiness;

  /*
   * piecesUrl : URL pour récupérer l'ensemble des pièces jouables
   */
  private piecesUrl = 'api/pieces/pieces.json';

  constructor(private http: HttpClient) {
    this.engineBusiness = new EngineBusiness();
  }

  /*
   * Création du plateau
   */
  creerPlateau(): Observable<Plateau> {
    return of(this.engineBusiness.creerPlateau());
  }

  /*
   * Vérification du plateau et calcul du score
   */
  verifierPlateau(plateau: Plateau): Observable<Plateau> {
    return of(this.engineBusiness.verifierPlateau(plateau));
  }

  /*
   * Récupération d'une pièce par son nom
   */
  getPiece(nom: string): Observable<IPiece | undefined> {
    return this.http.get<IPiece[]>(this.piecesUrl).pipe(
      map((pieces: IPiece[]) => pieces.find((piece: IPiece) => piece.nom === nom)),
    );
  }

  /*
   * Récupération des pièces jouables par les joueurs aléatoirement
   */
  getRandomPiece(): Observable<IPiece[] | undefined> {
    const randomIndex: number[] = this.engineBusiness.getRandomIndex();
    return this.http.get<IPiece[]>(this.piecesUrl).pipe(
      map((pieces: IPiece[]) => {
        let tmp = [];
        for (let i = 0; i < randomIndex.length; i++) {
          tmp.push(pieces[randomIndex[i]]);
        }
        return tmp;
      })
    );
  }

  /*
   * Vérification des pièces jouables pour savoir si la partie est terminée
   */
  verifierPieces(plateau: Plateau, pieces: IPiece[]): Observable<boolean> {
    return of(this.engineBusiness.verifierPieces(plateau, pieces));
  }
}
