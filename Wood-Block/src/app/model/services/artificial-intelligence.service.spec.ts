import { TestBed } from '@angular/core/testing';

import { ArtificialIntelligenceService } from './artificial-intelligence.service';

describe('ArtificialIntelligenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArtificialIntelligenceService = TestBed.get(ArtificialIntelligenceService);
    expect(service).toBeTruthy();
  });
});
