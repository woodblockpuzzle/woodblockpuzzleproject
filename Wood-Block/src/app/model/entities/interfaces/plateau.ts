export interface IPlateau {
  mainTable: number[][];
  score: number;
}
