import {IPieceIA} from './IPieceIA';

export interface IReponseIA {
  status: string;
  points: number;
  pieces: IPieceIA[];
}
