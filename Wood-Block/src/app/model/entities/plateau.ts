import {IPlateau} from './interfaces/plateau';

export class Plateau implements IPlateau {
  mainTable: number[][];
  score: number;
}
