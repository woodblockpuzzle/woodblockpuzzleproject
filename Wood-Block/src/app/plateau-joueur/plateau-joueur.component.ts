import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DropEvent } from 'ng-drag-drop';
import { Subscription, fromEvent } from 'rxjs';
import { EngineService } from '../model/services/engine.service';
import { IPlateau } from '../model/entities/interfaces/plateau';
import { trigger, state, style, animate, transition } from '@angular/animations';

import * as $ from 'jquery';
import { IPiece } from '../model/entities/interfaces/piece';
import { ComponentCommunicationService } from '../model/services/component-communication.service';

@Component({
  selector: 'plateau-joueur',
  templateUrl: './plateau-joueur.component.html',
  styleUrls: ['./plateau-joueur.component.scss'],
  // animations: [
  //   trigger('changeImgSrc', [
  //     state('initial', style({
  //       // width: '100px',
  //       // height: '100px'
  //     })),
  //     state('final', style({
  //       // width: '50px',
  //       // height: '50px'
  //     })),
  //     transition('initial=>final', animate('1500ms')),
  //     transition('final=>initial', animate('1000ms'))
  //   ]),
  // ]
})
export class PlateauJoueurComponent implements OnInit {

  @Input() public gridJoueur: IPlateau;
  @Output() public updateScoreJoueurEvent = new EventEmitter();

  private source: IPiece; // block posé
  private cible: Array<number>; // coordonnées de la case cible dans la grille

  private subscription: Subscription;

  cibleStateChanged: EventEmitter<Array<number>> = new EventEmitter();

  // currentState = 'initial';

  // changeState() {
  //   document.getElementById('0,0').classList.remove('hint');
  //   document.getElementById('0,0').classList.add('col-sm-1', 'border');
  //   this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
  // }

  constructor(private engineService: EngineService, private compComService: ComponentCommunicationService) {

    this.subscription = this.cibleStateChanged.subscribe(x => {
      // mettre à jour la grille si le déplacement est valide
      if (this.checkMoveValid()) {
        this.updateGrille(this.source, this.cible); // remplacer les 0 par 1
        this.compComService.deletePieceJoueur(this.source); // événement pour supprimer la piéce posée de la liste des piéces à poser
        // this.pieceDroppedEvent.emit(this.source); // événement pour supprimer la piéce posée de la liste des piéces à poser

        engineService.verifierPlateau(this.gridJoueur).subscribe(plateau => {

          this.gridJoueur = plateau;

            console.log("verfiferplateau success");
            this.updateScoreJoueurEvent.emit(this.gridJoueur.score);
            console.log(this.gridJoueur.mainTable);

            // animation pour enlever les blocks
            // this.changeState();
            // this.makeAnimation();
          });

        // console.log($('img'));
        // console.log(this.gridJoueur.score);
        // console.log(this.gridJoueur.mainTable);
      }
    });

    this.compComService.getPreview().subscribe((piece) => this.preview(piece));

  }

  ngOnInit() {
    // document.querySelector('.top')
  }

  onDragOver(x: any, y: any) {
    // try {
    //   let hint = document.querySelector('.hint');
    //   hint.classList.remove('hint');
    //   hint.setAttribute('class','col-sm-1 border');
    // } catch (error) {
    //   console.log(error);
    // }
    // console.log("drag over");
    // console.log(this.source);
    let values = this.source.values;
    for (let k = 0; k < values.length; k++) {
      for (let l = 0; l < values[k].length; l++) {
        if (values[k][l] == 1) {
          try {
            let div = document.getElementById(`${x + k},${y + l}`);
            div.setAttribute('class', 'col-sm-1 hint');
            // réinitialise la bordure après un court moment
            setTimeout(() => {
              div.classList.remove('hint');
              div.setAttribute('class', 'col-sm-1 border');
            }, 200);
          } catch (error) {
            console.log(error);
          }
        }
      }
    }
  }

  onDrop(e: DropEvent) {
    console.log(e.dragData);

    this.source = e.dragData;
  }

  dropped(e: Event) {
    let element = (<HTMLElement>(<HTMLElement>e.target).parentNode);
    let x = Number(element.id.split(',')[0]);
    let y = Number(element.id.split(',')[1]);
    this.cible = [x, y];
    this.cibleStateChanged.emit(this.cible);
  }

  // stateChangedEmitter() {
  //   if (this.sourceStateChanged && this.cibleStateChanged)
  //     console.log("source et cible updated");
  // }

  // retourne true si le block peut être posé
  checkMoveValid() {
    let values: Array<Array<number>> = this.source.values;
    let i: number = 0;
    while (i < values.length) {
      let k: number = this.cible[0] + i; // décaler par rapport aux lignes

      if (k >= 8) return false;

      let j: number = 0;

      while (j < values[i].length) {
        let l: number = this.cible[1] + j; // décaler par rapport aux colonnes

        try {
          if (l >= 8 || (values[i][j] == 1 && this.gridJoueur.mainTable[k][l] == 1)) return false;
          // if (l > 8 || (values[i][j] == 1 && this.gridJoueur.mainTable[k][l] != 0)) return false;

          j++;
        } catch (error) {
          console.log(error);
        }
      }
      i++;
    }

    return true;
  }

  preview(piece: any): void {
    this.source = piece;
  }

  // preview(piece: any): void {
  //   this.source = piece;
  //   let values: Array<Array<number>> = piece.values;

  //   for (let i = 0; i < 8; i++) {
  //     for (let j = 0; j < 8; j++) {
  //       if (this.checkBlockForPreview(i, j, values)) {
  //         // console.log(`${i},${j} : block valide pour preview`);
  //         // document.getElementById(`${i},${j}`).setAttribute('class', 'hint');
  //         for (let k = 0; k < values.length; k++) {
  //           for (let l = 0; l < values[k].length; l++) {
  //             if (values[k][l] == 1) {
  //               console.log(i + k)
  //               console.log(j + l)
  //             }
  //           }
  //         }
  //         // document.getElementById(`0,0`).setAttribute('class', 'col-sm-1 hint');

  //         // this.gridJoueur.mainTable[i][j] = 3;
  //       }
  //     }
  //   }
  // }

  // checkBlockForPreview(x: number, y: number, values: Array<Array<number>>): boolean {

  //   let i: number = 0;
  //   while (i < values.length) {
  //     let k: number = x + i; // décaler par rapport aux lignes

  //     if (k > 8) return false;

  //     let j: number = 0;

  //     while (j < values[i].length) {
  //       let l: number = y + j; // décaler par rapport aux colonnes

  //       try {
  //         if (l > 8 || (values[i][j] == 1 && this.gridJoueur.mainTable[k][l] != 0)) return false;

  //         j++;
  //       } catch (error) {
  //         return false;
  //       }
  //     }
  //     i++;
  //   }

  //   return true;
  // }

  // animate() {
  //   console.log("function animate");
  //   $("img[src$='-2.png']").fadeOut("slow", function () {
  //     $("img[src$='-2.png']").attr("src", "assets/img/0.png");
  //   }).fadeIn("slow");

  //   // console.log("button animation clicked");
  // }

  // animation pour la destruction de blocks
  // makeAnimation() {
  //   // console.log("make animation function");

  //   // let button: HTMLElement = document.getElementById('animate') as HTMLElement;
  //   // button.click();

  //   // this.gridJoueur = this.gridJoueur;
  //   // // remplacer les -2 par 0
  //   let arr = this.gridJoueur.mainTable;
  //   for (let i = 0; i < arr.length; i++)
  //     for (let j = 0; j < arr[i].length; j++)
  //       if (arr[i][j] == -2)
  //         arr[i][j] = 0;

  // }

  updateGrille(source: any, cible: any) {
    let values: Array<Array<number>> = source.values;

    for (let i: number = 0; i < values.length; i++) {
      let k: number = cible[0] + i; // décaler par rapport aux lignes
      for (let j: number = 0; j < values[i].length; j++) {
        let l: number = cible[1] + j; // décaler par rapport aux colonnes
        try {
          console.log(`k = ${k}, l = ${l}`);
          this.gridJoueur.mainTable[k][l] = values[i][j] ? values[i][j] : this.gridJoueur.mainTable[k][l];
        } catch (error) {
          console.log(error);
        }
      }
    }

    // remplacer -2 par 0 pour pas affecter le nouveau calcul du score
    for (let i: number = 0; i < this.gridJoueur.mainTable.length; i++) {
      for (let j: number = 0; j < this.gridJoueur.mainTable[i].length; j++) {
        this.gridJoueur.mainTable[i][j] = this.gridJoueur.mainTable[i][j] == -2 ? 0 : this.gridJoueur.mainTable[i][j];
      }
    }


    // this.destroyBlock();
    // this.removeItem(source);

  }

}